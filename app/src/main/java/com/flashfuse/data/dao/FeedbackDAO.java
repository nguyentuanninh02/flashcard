package com.flashfuse.data.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.flashfuse.data.SQLiteHelper;
import com.flashfuse.data.entity.Feedback;

import java.util.ArrayList;
import java.util.List;

public class FeedbackDAO {
    SQLiteHelper db;
    SQLiteDatabase sqLiteDatabase;

    public FeedbackDAO(Context context) {
        this.db = new SQLiteHelper(context);
        sqLiteDatabase = db.getWritableDatabase();
    }

    public void insertFeedback(String feedback){
        sqLiteDatabase = db.getWritableDatabase();
        String query = "INSERT INTO feedback (feedback) VALUES ('"+feedback+"')";
        sqLiteDatabase.execSQL(query);
    }

    public void deleteFeedback(int feedbackId){
        sqLiteDatabase = db.getWritableDatabase();
        String query = "DELETE FROM feedback WHERE id = "+feedbackId;
        sqLiteDatabase.execSQL(query);
    }

    public void updateFeedback(int feedbackId, String feedback){
        sqLiteDatabase = db.getWritableDatabase();
        String query = "UPDATE feedback SET feedback = '"+feedback+"' WHERE id = "+feedbackId;
        sqLiteDatabase.execSQL(query);
    }

    public List<Feedback> getAll(){
        List<Feedback> list= new ArrayList<>();
        String query = "SELECT * FROM feedback ORDER BY created_at DESC";
        try (Cursor cursor = sqLiteDatabase.rawQuery(query, null)) {
            if (cursor.moveToFirst()) {
                do {
                    Feedback feedback = new Feedback();
                    feedback.setId(cursor.getInt(0));
                    feedback.setUser_id(cursor.getInt(1));
                    feedback.setTitle(cursor.getString(2));
                    feedback.setContent(cursor.getString(3));
                    feedback.setCreated_at(cursor.getString(4));
                    feedback.setUpdate_at(cursor.getString(5));
                    list.add(feedback);
                } while (cursor.moveToNext());
            }
        }
        return list;
    }


    public void getFeedbackById(int feedbackId){
        sqLiteDatabase = db.getReadableDatabase();
        String query = "SELECT * FROM feedback WHERE id = "+feedbackId;
        sqLiteDatabase.execSQL(query);
    }

    //add
    public void addFeedback(Feedback f){
        ContentValues values= new ContentValues();
        values.put("title", f.getTitle());
        values.put("content", f.getContent());
        values.put("user_id", f.getUser_id());
        values.put("created_at", f.getCreated_at());
        values.put("updated_at", f.getUpdate_at());
        SQLiteDatabase st= db.getWritableDatabase();
        st.insert("feedback", null, values);
    }

    public List<Feedback> getFeedbackByUserId(int userId){
        List<Feedback> list= new ArrayList<>();
        String query = "SELECT * FROM feedback WHERE user_id = "+userId;
        try (android.database.Cursor cursor = sqLiteDatabase.rawQuery(query, null)) {
            if (cursor.moveToFirst()) {
                do {
                    Feedback feedback = new Feedback();
                    feedback.setId(cursor.getInt(0));
                    feedback.setUser_id(cursor.getInt(1));
                    feedback.setTitle(cursor.getString(2));
                    feedback.setContent(cursor.getString(3));
                    feedback.setCreated_at(cursor.getString(4));
                    feedback.setUpdate_at(cursor.getString(5));
                    list.add(feedback);
                } while (cursor.moveToNext());
            }
        }
        return list;
    }

    public String getUsernameOfFeedback(int feedbackId){
        String query = "SELECT user.name FROM feedback INNER JOIN user ON feedback.user_id = user.id WHERE feedback.id = "+feedbackId;
        try (android.database.Cursor cursor = sqLiteDatabase.rawQuery(query, null)) {
            if (cursor.moveToFirst()) {
                return cursor.getString(0);
            }
        }
        return null;
    }

}
