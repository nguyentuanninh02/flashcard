package com.flashfuse.data.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.flashfuse.data.SQLiteHelper;
import com.flashfuse.data.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserDAO {
    SQLiteHelper db;
    SQLiteDatabase sqLiteDatabase;

    public UserDAO(Context context) {
        this.db = new SQLiteHelper(context);
        sqLiteDatabase = db.getWritableDatabase();
    }

    public int getUserUsername(String username){
        sqLiteDatabase = db.getReadableDatabase();
        String query = "SELECT id FROM user WHERE username = '"+username+"'";
        Cursor rs = sqLiteDatabase.rawQuery(query, null);
        if (rs != null && rs.moveToFirst()) {
            return rs.getInt(0);
        } else {
            return -1; // return a default value or throw an exception
        }
    }

    public User getUserById(int id){
        sqLiteDatabase = db.getReadableDatabase();
        String query = "SELECT * FROM user WHERE id = '"+id+"'";
        Cursor rs = sqLiteDatabase.rawQuery(query, null);
        if(rs!= null && rs.moveToNext()){
            String username= rs.getString(1);
            String password= rs.getString(2);
            String email= rs.getString(3);
            String name= rs.getString(4);
            String phone= rs.getString(5);
            int role= rs.getInt(6);
            int status= rs.getInt(7);
            return new User(id, username, password, email, name, phone, role, status);
        }
        return null;
    }


    //check login, input username and password and status= 1
    public boolean checkLogin(String username, String password){
        sqLiteDatabase = db.getReadableDatabase();
        String query = "SELECT * FROM user WHERE username = '"+username+"' AND password = '"+password+"' AND status = 1";
        Cursor rs = sqLiteDatabase.rawQuery(query, null);
        return rs!= null && rs.moveToNext();
    }

    //check User role 0: admin, 1: user
    public int CheckUserRole(String username, String password){
        sqLiteDatabase = db.getReadableDatabase();
        String query = "SELECT role FROM user WHERE username = '"+username+"' AND password = '"+password+"'";
        Cursor rs = sqLiteDatabase.rawQuery(query, null);
        if (rs != null && rs.moveToFirst()) {
            return rs.getInt(0);
        } else {
            return -1; // return a default value or throw an exception
        }
    }

    //update User
    public void updateUser(User user){
        sqLiteDatabase = db.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("username", user.getUsername());
        contentValues.put("password", user.getPassword());
        contentValues.put("email", user.getEmail());
        contentValues.put("name", user.getName());
        contentValues.put("phone", user.getPhone());
        contentValues.put("role", user.getRole());
        contentValues.put("status", user.getStatus());
        sqLiteDatabase.update("user", contentValues, "id = ?", new String[]{String.valueOf(user.getId())});
    }

    //search user by username or gmail or phone or name and role = 1
    public List<User> searchUser(String keyword){
        sqLiteDatabase = db.getReadableDatabase();
        String query = "SELECT * FROM user WHERE (username LIKE '%"+keyword+"%' OR email LIKE '%"+keyword+"%' OR phone LIKE '%"+keyword+"%' OR name LIKE '%"+keyword+"%') AND role = 1 AND status = 1";
        Cursor rs = sqLiteDatabase.rawQuery(query, null);
        List<User> list = new ArrayList<>();
        while (rs!= null && rs.moveToNext()){
            int id= rs.getInt(0);
            String username= rs.getString(1);
            String password= rs.getString(2);
            String email= rs.getString(3);
            String name= rs.getString(4);
            String phone= rs.getString(5);
            int role= rs.getInt(6);
            int status= rs.getInt(7);
            list.add(new User(id, username, password, email, name, phone, role, status));
        }
        return list;
    }

    //insert User
    public void insert(String username, String password, String email, String name, String phone){
        sqLiteDatabase = db.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("username", username);
        contentValues.put("password", password);
        contentValues.put("email", email);
        contentValues.put("name", name);
        contentValues.put("phone", phone);
        contentValues.put("role", 1);
        contentValues.put("status", 1);
        sqLiteDatabase.insert("user", null, contentValues);
    }

    //check username exist
    public boolean checkUsernameExist(String username){
        sqLiteDatabase = db.getReadableDatabase();
        String query = "SELECT * FROM user WHERE username = '"+username+"'";
        Cursor rs = sqLiteDatabase.rawQuery(query, null);
        return rs!= null && rs.moveToNext();
    }

}
