package com.flashfuse.fragment.admin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.flashfuse.R;
import com.flashfuse.activity.LoginActivity;
import com.flashfuse.activity.user.ChangePasswordActivity;
import com.flashfuse.activity.user.EditProfileActivity;
import com.flashfuse.data.dao.UserDAO;
import com.flashfuse.data.entity.User;


public class AdminAccountFragment extends Fragment {

    TextView usernameTextView, name, emailTextView,phone;
    Button editProfileButton, logoutButton, editChangePasswordButton;

    UserDAO userDAO;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_admin_account, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        usernameTextView = view.findViewById(R.id.usernameTextView);
        name = view.findViewById(R.id.name);
        emailTextView = view.findViewById(R.id.emailTextView);
        phone = view.findViewById(R.id.phone);
        editProfileButton = view.findViewById(R.id.editProfileButton);
        logoutButton = view.findViewById(R.id.logoutButton);
        editChangePasswordButton = view.findViewById(R.id.editChangePasswordButton);
        userDAO= new UserDAO(getContext());

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("LoginState", Context.MODE_PRIVATE);
        int userId = sharedPreferences.getInt("userId", 0);
        User user = userDAO.getUserById(userId);

        usernameTextView.setText(user.getUsername());
        name.setText(user.getName());
        emailTextView.setText(user.getEmail());
        phone.setText(user.getPhone());

        editProfileButton.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), EditProfileActivity.class);
            startActivity(intent);
        });

        editChangePasswordButton.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), ChangePasswordActivity.class);
            startActivity(intent);
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Xóa trạng thái đăng nhập
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences("LoginState", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.apply();

                // Chuyển người dùng về trang đăng nhập
                Intent intent = new Intent(getContext(), LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("LoginState", Context.MODE_PRIVATE);
        int userId = sharedPreferences.getInt("userId", 0);
        User user = userDAO.getUserById(userId);
        usernameTextView.setText(user.getUsername());
        name.setText(user.getName());
        emailTextView.setText(user.getEmail());
        phone.setText(user.getPhone());
    }
}