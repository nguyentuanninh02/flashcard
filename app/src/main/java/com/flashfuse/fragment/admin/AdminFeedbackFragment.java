package com.flashfuse.fragment.admin;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flashfuse.R;
import com.flashfuse.adapter.FeedbackRecycleView;
import com.flashfuse.data.dao.FeedbackDAO;
import com.flashfuse.data.entity.Feedback;

import java.util.List;

public class AdminFeedbackFragment extends Fragment {

    RecyclerView recyclerView;
    FeedbackRecycleView adapter;
    List<Feedback> list;

    FeedbackDAO feedbackDAO;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_admin_feedback, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.search_results);
        feedbackDAO = new FeedbackDAO(getContext());
        list = feedbackDAO.getAll();
        adapter = new FeedbackRecycleView(getContext(), list);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager manager= new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
    }

    @Override
    public void onResume() {
        super.onResume();
        list = feedbackDAO.getAll();
        adapter.setList(list);
        adapter.notifyDataSetChanged();
    }
}