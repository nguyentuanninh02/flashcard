package com.flashfuse.fragment.user;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.flashfuse.R;
import com.flashfuse.activity.IntroduceActivity;
import com.flashfuse.activity.LoginActivity;
import com.flashfuse.activity.feedback.SendFeedbackActivity;
import com.flashfuse.activity.user.AccountInfoActivity;
import com.flashfuse.data.dao.UserDAO;
import com.flashfuse.data.entity.User;


public class ProfileFragment extends Fragment {

    Button btn_account_info, btn_send_feedback, btn_introduce, btn_delete_account, btn_logout;
    UserDAO userDAO;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btn_account_info = view.findViewById(R.id.btn_account_info);
        btn_send_feedback = view.findViewById(R.id.btn_send_feedback);
        btn_introduce = view.findViewById(R.id.btn_introduce);
        btn_delete_account = view.findViewById(R.id.btn_delete_account);
        btn_logout = view.findViewById(R.id.btn_logout);
        userDAO= new UserDAO(getContext());

        btn_account_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(getActivity(), AccountInfoActivity.class);
                startActivity(intent);
            }
        });

        btn_send_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(getActivity(), SendFeedbackActivity.class);
                startActivity(intent);
            }
        });

        btn_introduce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(getActivity(), IntroduceActivity.class);
                startActivity(intent);
            }
        });

        btn_delete_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getContext())
                        .setTitle("Delete Account")
                        .setMessage("Are you sure you want to delete your account?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                // Xóa trạng thái đăng nhập
                                SharedPreferences sharedPreferences = getActivity().getSharedPreferences("LoginState", Context.MODE_PRIVATE);
                                int userId = sharedPreferences.getInt("userId", -1);
                                User user = userDAO.getUserById(userId);
                                user.setStatus(0);
                                userDAO.updateUser(user);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.clear();
                                editor.apply();

                                // Chuyển người dùng về trang đăng nhập
                                Intent intent = new Intent(getContext(), LoginActivity.class);
                                startActivity(intent);

                                // Hiển thị thông báo cho người dùng biết tài khoản của họ đã bị xóa
                                Toast.makeText(getContext(), "Your account has been deleted", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Xóa trạng thái đăng nhập
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences("LoginState", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.apply();

                // Chuyển người dùng về trang đăng nhập
                Intent intent = new Intent(getContext(), LoginActivity.class);
                startActivity(intent);
            }
        });
    }
}