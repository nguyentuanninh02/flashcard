package com.flashfuse.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.flashfuse.R;
import com.flashfuse.activity.admin.UserDetailActivity;
import com.flashfuse.data.entity.User;

import java.util.List;

public class UserRecycleAdapter extends RecyclerView.Adapter<UserRecycleAdapter.ViewHolder>{
    List<User> list;
    Context context;

    public void setList(List<User> list) {
        this.list = list;
    }

    public UserRecycleAdapter(Context context, List<User> list) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User user= list.get(position);
        holder.userName.setText(user.getUsername());
        holder.userEmail.setText(user.getEmail());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView userName, userEmail;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            userName= itemView.findViewById(R.id.userName);
            userEmail= itemView.findViewById(R.id.userEmail);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent= new Intent(context, UserDetailActivity.class);
                    intent.putExtra("user", list.get(getAdapterPosition()));
                    context.startActivity(intent);
                }
            });
        }
    }
}
