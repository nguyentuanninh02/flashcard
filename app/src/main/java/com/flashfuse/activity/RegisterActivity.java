package com.flashfuse.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.flashfuse.R;
import com.flashfuse.data.dao.UserDAO;

public class RegisterActivity extends AppCompatActivity {

    EditText username, password, name, email, phone;

    TextView signupText;
    Button btn_register;

    UserDAO userDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        signupText = findViewById(R.id.signupText);
        btn_register = findViewById(R.id.btn_register);

        userDAO = new UserDAO(this);

        signupText.setOnClickListener(v -> {
            finish();
        });

        btn_register.setOnClickListener(v -> {
            String usernameStr = username.getText().toString();
            String passwordStr = password.getText().toString();
            String nameStr = name.getText().toString();
            String emailStr = email.getText().toString();
            String phoneStr = phone.getText().toString();

            if (usernameStr.isEmpty() || passwordStr.isEmpty() || nameStr.isEmpty() || emailStr.isEmpty() || phoneStr.isEmpty()) {
                return;
            }
            if(userDAO.checkUsernameExist(usernameStr)){
                Toast.makeText(this, "Username already exists", Toast.LENGTH_SHORT).show();
                return;
            }
            userDAO.insert(usernameStr, passwordStr, emailStr,nameStr, phoneStr);
            Toast.makeText(this, "Register successfully", Toast.LENGTH_SHORT).show();
            finish();
        });

    }
}