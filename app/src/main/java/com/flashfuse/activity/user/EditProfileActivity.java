package com.flashfuse.activity.user;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.flashfuse.R;
import com.flashfuse.data.dao.UserDAO;
import com.flashfuse.data.entity.User;

public class EditProfileActivity extends AppCompatActivity {
    EditText username, name, email, phone;

    Button btn_save, btn_cancel;

    UserDAO userDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        username = findViewById(R.id.username);
        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        btn_save = findViewById(R.id.btn_save);
        btn_cancel = findViewById(R.id.btn_cancel);
        userDAO = new UserDAO(this);

        //get user info
        SharedPreferences sharedPreferences = getSharedPreferences("LoginState", MODE_PRIVATE);
        int userId = sharedPreferences.getInt("userId", 0);
        User user = userDAO.getUserById(userId);

        //set user info to edit text
        username.setText(user.getUsername());
        name.setText(user.getName());
        email.setText(user.getEmail());
        phone.setText(user.getPhone());

        btn_save.setOnClickListener(v -> {
            String usernameStr = username.getText().toString();
            String nameStr = name.getText().toString();
            String emailStr = email.getText().toString();
            //validate email
            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailStr).matches()) {
                email.setError("Invalid email");
                return;
            }
            String phoneStr = phone.getText().toString();
            user.setUsername(usernameStr);
            user.setName(nameStr);
            user.setEmail(emailStr);
            user.setPhone(phoneStr);
            userDAO.updateUser(user);
            finish();
        });

        btn_cancel.setOnClickListener(v -> {
            finish();
        });

    }
}