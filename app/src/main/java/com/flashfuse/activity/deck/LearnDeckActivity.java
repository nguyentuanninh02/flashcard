package com.flashfuse.activity.deck;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.flashfuse.R;
import com.flashfuse.data.dao.CardDAO;
import com.flashfuse.data.entity.Card;

import java.util.List;

public class LearnDeckActivity extends AppCompatActivity {
    private AnimatorSet mSetRightOut;
    private AnimatorSet mSetLeftIn;

    private TextView cardTextView, progressTextView;
    private Button nextButton, prevButton;
    ImageView backButton;
    FrameLayout frameLayout;
    private CardView cardView;
    private List<Card> cards;
    private int currentCardIndex = 0;
    private boolean isShowingFront = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn_deck);

        cardTextView = findViewById(R.id.cardTextView);
        progressTextView = findViewById(R.id.progressTextView);
        nextButton = findViewById(R.id.nextButton);
        prevButton = findViewById(R.id.prevButton);
        cardView = findViewById(R.id.cardView);
        frameLayout = findViewById(R.id.cardFrameLayout);

        long deckId = getIntent().getLongExtra("DECK_ID", -1);
        CardDAO cardDAO = new CardDAO(this);
        cards = cardDAO.getCardByDeckId((int)deckId);

        if (!cards.isEmpty()) {
            displayCard();
            updateProgress();
        }

        //animation


        loadAnimations();
        changeCameraDistance();

        //flip frameLayout when click

        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isShowingFront) {
                    mSetRightOut.setTarget(frameLayout);
                    mSetLeftIn.setTarget(frameLayout);
                    mSetRightOut.start();
                    mSetLeftIn.start();
                    isShowingFront = true;
                } else {
                    mSetRightOut.setTarget(frameLayout);
                    mSetLeftIn.setTarget(frameLayout);
                    mSetRightOut.start();
                    mSetLeftIn.start();
                    isShowingFront = false;
                }
                displayCard();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentCardIndex < cards.size() - 1) {
                    currentCardIndex++;
                    isShowingFront = true;
                    displayCard();
                    updateProgress();
                }
            }
        });

        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentCardIndex > 0) {
                    currentCardIndex--;
                    isShowingFront = true;
                    displayCard();
                    updateProgress();
                }
            }
        });

        backButton = findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void displayCard() {
        Card currentCard = cards.get(currentCardIndex);
        if (isShowingFront) {
            cardTextView.setText(currentCard.getFront());
        } else {
            cardTextView.setText(currentCard.getBack());
        }
    }

    private void updateProgress() {
        String progress = (currentCardIndex + 1) + "/" + cards.size();
        progressTextView.setText(progress);
    }

    private void loadAnimations() {
        mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.out_animation);
        mSetLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.in_animation);
    }

    private void changeCameraDistance() {
        int distance = 8000;
        float scale = getResources().getDisplayMetrics().density * distance;
        cardView.setCameraDistance(scale);
    }
}