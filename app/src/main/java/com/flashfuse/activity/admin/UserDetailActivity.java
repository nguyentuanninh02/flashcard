package com.flashfuse.activity.admin;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.flashfuse.R;
import com.flashfuse.data.dao.UserDAO;
import com.flashfuse.data.entity.User;

public class UserDetailActivity extends AppCompatActivity {

    TextView usernameTextView, name, emailTextView,phone;

    Button btn_reset_password,btn_delete_account, btn_cancel;

    User user;
    UserDAO userDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);

        usernameTextView = findViewById(R.id.usernameTextView);
        name = findViewById(R.id.name);
        emailTextView = findViewById(R.id.emailTextView);
        phone = findViewById(R.id.phone);
        btn_reset_password = findViewById(R.id.btn_reset_password);
        btn_delete_account = findViewById(R.id.btn_delete_account);
        btn_cancel = findViewById(R.id.btn_cancel);
        userDAO = new UserDAO(UserDetailActivity.this);

        Intent intent = getIntent();
        if(intent.getSerializableExtra("user")!= null){
            user = (User) intent.getSerializableExtra("user");
        }

        usernameTextView.setText(user.getUsername());
        name.setText(user.getName());
        emailTextView.setText(user.getEmail());
        phone.setText(user.getPhone());

        btn_reset_password.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(UserDetailActivity.this);
            builder.setTitle("Confirm Password Reset");
            builder.setMessage("Are you sure you want to reset the password?");

            builder.setPositiveButton("Yes", (dialog, which) -> {
                user.setPassword("123456");
                Toast.makeText(UserDetailActivity.this, "Password reset successful", Toast.LENGTH_SHORT).show();
                userDAO.updateUser(user);
            });

            builder.setNegativeButton("No", (dialog, which) -> {
                dialog.dismiss();
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        });

        btn_delete_account.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(UserDetailActivity.this);
            builder.setTitle("Confirm Account Deletion");
            builder.setMessage("Are you sure you want to delete this account?");

            builder.setPositiveButton("Yes", (dialog, which) -> {
                user.setStatus(0);
                userDAO.updateUser(user);
                Toast.makeText(UserDetailActivity.this, "Delete User successful", Toast.LENGTH_SHORT).show();
                finish();
            });

            builder.setNegativeButton("No", (dialog, which) -> {
                dialog.dismiss();
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        });

        btn_cancel.setOnClickListener(v -> {
            finish();
        });


    }
}