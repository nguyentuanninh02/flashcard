package com.flashfuse.activity.feedback;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.flashfuse.R;
import com.flashfuse.data.dao.FeedbackDAO;
import com.flashfuse.data.entity.Feedback;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SendFeedbackActivity extends AppCompatActivity {

    EditText et_title, contentEditText;

    Button saveButton, cancelButton;

    FeedbackDAO feedbackDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_feedback);

        et_title = findViewById(R.id.et_title);
        contentEditText = findViewById(R.id.contentEditText);
        saveButton = findViewById(R.id.saveButton);
        cancelButton = findViewById(R.id.cancelButton);
        feedbackDAO = new FeedbackDAO(this);

        SharedPreferences sharedPreferences = getSharedPreferences("LoginState", MODE_PRIVATE);
        int userId = sharedPreferences.getInt("userId", 0);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = et_title.getText().toString();
                String content = contentEditText.getText().toString();
                //get current date
                Date date = new Date();
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                String strDate = formatter.format(date);
                Feedback feedback= new Feedback(title, content, userId, strDate, "");
                System.out.println("Feedback: "+feedback.toString());
                feedbackDAO.addFeedback(feedback);
                Toast.makeText(SendFeedbackActivity.this, "Feedback sent", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}