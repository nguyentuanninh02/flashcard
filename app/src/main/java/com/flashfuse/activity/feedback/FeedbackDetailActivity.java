package com.flashfuse.activity.feedback;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.flashfuse.R;
import com.flashfuse.data.dao.FeedbackDAO;
import com.flashfuse.data.entity.Feedback;

public class FeedbackDetailActivity extends AppCompatActivity {

    TextView title,username,date, content;
    Feedback feedback;
    Button btn_cancel;

    FeedbackDAO feedbackDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_detail);

        Intent intent= getIntent();
        feedback= (Feedback) intent.getSerializableExtra("feedback");

        title= findViewById(R.id.title);
        username= findViewById(R.id.username);
        date= findViewById(R.id.date);
        content= findViewById(R.id.content);
        btn_cancel= findViewById(R.id.btn_cancel);
        feedbackDAO= new FeedbackDAO(this);

        title.setText(feedback.getTitle());
        username.setText(feedbackDAO.getUsernameOfFeedback(feedback.getId()));
        date.setText(feedback.getCreated_at());
        content.setText(feedback.getContent());

        btn_cancel.setOnClickListener(v -> {
            finish();
        });
    }
}